function reload() {
    var e = document.getElementById("languages");
    var selectedLang = e.options[e.selectedIndex].value;

    var url = "?lang=" + selectedLang
    location.href = url;
}

function loadData() {
    if (isNaN(document.getElementById('year').value)) {
        deleteData()
        var language = document.cookie.split('=')[1]
        switch (language) {
            case "es":
                alert("Comprueba los parámetros")
                break;
            case "en":
                alert("Ccheck the params")
                break;
            default:
                alert("Chek the params")
                break;
        }
        return
    }

    let controllerURL = "http://localhost:9191/movie"
    let search = 0
    if ((document.getElementById('title').value != "") && (document.getElementById('year').value == "")) {
        search = 1
    }
    if ((document.getElementById('title').value == "") && (document.getElementById('year').value != "")) {
        search = 2
    }
    if ((document.getElementById('title').value != "") && (document.getElementById('year').value != "")) {
        search = 3
    }
    if ((document.getElementById('title').value == "") && (document.getElementById('year').value == "")) {
        search = 0
    }

    switch (search) {
        case 1:
            controllerURL += "?title=" + document.getElementById('title').value
            break;
        case 2:
            controllerURL += '?year=' + document.getElementById('year').value
            break;
        case 3:
            controllerURL += "?title=" + document.getElementById('title').value + '&year=' + document.getElementById('year').value
            break;
        default:
            controllerURL = "http://localhost:9191/movie"
            break;
    }
    document.getElementById('infoPelis').innerHTML = ""

    fetch(controllerURL)
        .then(resultadoRaw => {

            return resultadoRaw.json();

        })
        .then(resultadoComoJson => {

            document.getElementById("infoPelis").style.visibility = "visible"
            document.getElementById("resultsText").style.visibility = "visible"
            if (JSON.stringify(resultadoComoJson) == "") {
                document.getElementById('infoPelis').innerHTML += "No records"
            } else {
                document.getElementById('infoPelis').innerHTML += JSON.stringify(resultadoComoJson)
            }

        });


}

function deleteData() {
    document.getElementById('infoPelis').innerHTML = ""
    document.getElementById("title").value = ""
    document.getElementById("year").value = ""
    document.getElementById("infoPelis").style.visibility = "hidden"
    document.getElementById("resultsText").style.visibility = "hidden"
}