package jorge.munoz.API;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jorge.munoz.Model.Movie;

@RestController
public class MoviesController {


    private static ArrayList<Movie> moviesList = new ArrayList<Movie>() {
        {
            add(new Movie(1, "Lagrimas de sangre", 2009, "http://loremflickr.com/320/240/movie"));
            add(new Movie(2, "Lo imposible", 2015, "http://loremflickr.com/320/240/movie"));
            add(new Movie(2, "Lo mejor", 2016, "http://loremflickr.com/320/240/movie"));
            add(new Movie(3, "007. Imparable", 2015, "http://loremflickr.com/320/240/movie"));
            add(new Movie(4, "Chuck Norris revenge", 1990, "http://loremflickr.com/320/240/movie"));
            add(new Movie(5, "Chuck Norris revenge 2", 2000, "http://loremflickr.com/320/240/movie"));
            add(new Movie(6, "Homeland Security", 2002, "http://loremflickr.com/320/240/movie"));
        }
    };

    private Movie getMovieById(Integer id) {
        for (Movie movie : moviesList) {
            if (movie.getId() == (int) id) {
                return movie;
            }
        }
        return null;
    }

    /**
    * Get Movie(s) from a list. 
    * @author JorgeM
    * @param title Title of the movie to search (Optional)
    * @return ArrayList with all movies if no Params, or arraylist with the movies or movie founded.
    * @throws NotDataFoundException 
    */
    private ArrayList<Movie> getMovieByTitle(String title) {
        boolean foundTitle = false;
        if (title == null) {
            return moviesList;
        }
        ArrayList<Movie> moviesByTitle = new ArrayList<>();
        for (Movie movie : moviesList) {
            String movieTitleLower = movie.getTitle().toLowerCase();
            String movieSerachTitleLower = title.toLowerCase();
            if (movieTitleLower.contains(movieSerachTitleLower)) {
                moviesByTitle.add(movie);
                foundTitle = true;
            }
        }
        if (foundTitle)
            return moviesByTitle;
        return null;
    }


    /**
    * Get Movie(s) from a list. 
    * @author JorgeM
    * @param list All list of movies or already filtered by title
    * @param yr Year of the movie (Optional)
    * @return ArrayList with all movies if no Params, or arraylist with the movies or movie founded.
    * @throws NotDataFoundException 
    */
    private ArrayList<Movie> getMovieByYear(ArrayList<Movie> list, Integer yr) {

        boolean foundYear = false;
        ArrayList<Movie> movies = new ArrayList<>();

        if (yr == null) {
            return list;
        }
        for (Movie movie : list) {
            if (movie.getYear() == (int) yr) {
                movies.add(movie);
                foundYear = true;
            }
        }
        if (foundYear)
            return movies;
        return null;
    }
    
    @GetMapping("/movie")
    public ArrayList<Movie> getMovies(@RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "year", required = false) Integer year) {

        if ((title != null) && (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        if ((title != null) || (year != null)) {
            ArrayList<Movie> moviesFilteredTitle = new ArrayList<>();
            moviesFilteredTitle = getMovieByTitle(title);
            return getMovieByYear(moviesFilteredTitle, year);
        }
        return moviesList;

    }

    /*
    CRUD METHODS
    */
    @PostMapping("/movie")
    public Movie AddMovie(@RequestBody Movie newMovie) {
        if (getMovieById(newMovie.getId()) != null) {
            return null;
        } else {
            Movie tmpMovie = new Movie();
            tmpMovie.setId(newMovie.getId());
            tmpMovie.setTitle(newMovie.getTitle());
            tmpMovie.setYear(newMovie.getYear());
            tmpMovie.setPoster(newMovie.getPoster());
            moviesList.add(tmpMovie);
            return newMovie;
        }
    }

    @PutMapping("/movie")
    public Movie UpdateMovie(@RequestBody Movie newMovie) {
        if (getMovieById(newMovie.getId()) == null) {
            return null;
        } else {
            if (newMovie.getYear() != 0)
                getMovieById(newMovie.getId()).setTitle(newMovie.getTitle());
            if (newMovie.getTitle() != null)
                getMovieById(newMovie.getId()).setTitle(newMovie.getTitle());
            if (newMovie.getYear() != 0)
                getMovieById(newMovie.getId()).setYear(newMovie.getYear());
            if (newMovie.getPoster() != null)
                getMovieById(newMovie.getId()).setPoster(newMovie.getPoster());
            return getMovieById(newMovie.getId());
        }
    }

    @DeleteMapping("/movie/{id}")
    public void Delete(@PathVariable("id") int id) {
        if (getMovieById(id) != null)
            moviesList.remove(getMovieById(id));
    }

}
